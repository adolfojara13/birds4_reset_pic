#include <16F1789.h>                                                               //Libreria del dispositivo
#fuses NOWDT, NOBROWNOUT, NOPROTECT                                              //Fusibles: Sin WatchDog Timer, sin brownout, sin proteccion de memoria de programa
#include <PIC16F1789_registers.h>
#device ADC=8;                                                                   //Selecciona conversion A/D de 8 bits
#use delay(crystal=16MHz,clock=16MHz)                                            //clock=indica al compilador el valor del clock del procesador y el tipo de clock

#use rs232(baud=9600,parity=N,xmit=PIN_B6,rcv=PIN_B7,bits=8,stream=PORT1)        //Configuracion rs232, PORT1
#use rs232(baud=9600,parity=N,xmit=PIN_D4,rcv=PIN_D5,bits=8,stream=MPIC)         //Configuracion rs232, MPIC


int8 sec = 0;                                                                    //sec de 0 255
int8 min = 0;
int8 hou = 0;
unsigned int16 day_num = 0;                                                      //day_num de 0 a 65535
int16 main_count = 0;                                                            //contador principal 
unsigned int16 batvol = 0;                                                       //batvol de 0 a 65535
int8 resetData[11] = {0};                                                        //vector con el que se envia los datos al MAIN PIC
int8 MAIN_ACK;
int8 res_flag = 0;                                                               //bandera para el reseteo cada 24horas
int16 modev = 2;

#include<power_enable.c>                                                         //Se incluye la libreria power_enable
#include<read_current.c>                                                         //Se incluye la libreria read_current
#include<define_fun.c>                                                           //Se incluye la libreria define_fun

#INT_TIMER0                                                                      //Funcion de interrupcion del TMR0
void TIMER0_isr() {                                                              //Envia un pulso al WDT externo

if(!get_timer0()){                                                               //Si lectura de TMR0 es cero
               RB2 = 1;                                                          //Pulso de 5us en RB2
               delay_us(5);
               RB2 = 0; 
           }
     
}

#INT_TIMER1                                                                      //Funcion de interrupcion del TMR1
void TIMER1_isr() {                                                              //Se utiliza para generar hhmmss
      set_timer1(0x8000);                                                        //Carga del timer1 para interrupcion cada 1 segundo
      main_count++;                                                              //Incrementa el contador principal

      if (sec < 59){
      sec++;
      }else{
         sec = 0;
         min++;
      }
      if (min == 60){
         min = 0;
         hou++;
      }
      
      if (hou == 24){                                                            //Si se cumple 24hs
         hou = 0;                                                                //Resetea la variable hora
         day_num++;                                                              //Incrementa la variable dia
         res_flag = 1;                                                           //pone en alto la bandera para reseteo a las 24hs
      }
      
      fprintf(PORT1,"%02ld", day_num);                                                  //Se imprime ddhhmmss
      fprintf(PORT1,":%02d", hou);
      fprintf(PORT1,":%02d", min);
      fprintf(PORT1,":%02d\r\n", sec);
      fprintf(PORT1,"maincount:%04ld\r\n", main_count);                                 //Imprime contador principal

}

void main()
{
                                                                                 //Configuracion de pines In/Out
   TRISC2 = 0; RC2 = 0;                                                          //Unreg#1 Enable (OCP 4A)                                                        
   TRISC3 = 0; RC3 = 0;                                                          //5V0 Enable (OCP 2A)
   TRISC4 = 0; RC4 = 0;                                                          //3V3#1 ON (converter)
   TRISC5 = 0; RC5 = 0;                                                          //Unreg#2 Enable (OCP 3A)
   
   TRISD0 = 0; RD0 = 0;                                                          //3V3#2 Enable (OCP 2A)
   TRISD1 = 0; RD1 = 0;                                                          //3V3#1 Enable (OCP 2A)
   TRISD2 = 0; RD2 = 0;                                                          //5V0 ON (converter)
   TRISD3 = 0; RD3 = 0;                                                          //3V3#2 ON (converter)
   TRISD6 = 0; RD6 = 0;                                                          //COM PIC Enable
   TRISD7 = 0; RD7 = 0;                                                          //MAIN PIC Enable
   
   TRISB2 = 0; RB2 = 0;                                                          //Al WDT externo
   TRISB5 = 0; RB5 = 1;                                                          //RAW voltage monitor ON
   
   SETUP_ADC(ADC_CLOCK_INTERNAL);                                                //Se selecciona la fuente del Clock utilizado para la conversion
   SETUP_ADC_PORTS(sAN0|sAN1|sAN2|sAN3|sAN4|sAN5|sAN6);                          //Definicion de las entradas analogicas
   
   clear_interrupt(int_timer1);
   setup_timer_1(T1_EXTERNAL | T1_DIV_BY_1);                                     //Config TMR1, clock externo, prescaler=1
   set_timer1(0x8000);                                                           //Carga del timer1 para interrupcion cada 1 segundo
   T1OSCEN = 1;                                                                  //Oscilador del TMR1 habilitado
   enable_interrupts(INT_TIMER1);                                                //Habilitacion de interrupcion TMR1
   enable_interrupts(INT_TIMER0);                                                //Habilitacion de interrupcion TMR0
   enable_interrupts(GLOBAL);                                                    //Habilitacion de interrupciones Globales
   setup_timer_0(RTCC_DIV_256 | RTCC_INTERNAL);                                  //Config TMR0, prescaler=256, uso reloj interno   
   set_timer0(0xFF);                                                             //Carga del TMR0
                                                                                 //Interrupcion cada 64us
   printf("*********************************************************\n\r");
   printf("********    BIRDS4 RESET PIC BBM VERSION      ***********\n\r");
   printf("*********************************************************\n\r");

   day_read();                                                                   //Funcion que carga el vector dy[] con el dato "dia" leido de la EEPROM
   
   if(make16( read_eeprom(day1H),read_eeprom(day1L) ) == -1)                     //Si al leer la EEPROM devuelve -1, es por que aun no tiene dato
   {
      day_num = 0;                                                               //Se inicializa como dia cero
      day_num_write();                                                           //Guarda en la EEPROM
   }
   
   
   if (dy[0]==dy[1] && dy[1]==dy[2] && dy[2]==dy[3] && dy[3]==dy[4] && dy[4]==dy[5] && dy[5]==dy[6] && dy[6]==dy[7] && dy[7]==dy[8] && dy[8]==dy[9])
   {
      day_num = dy[0];                                                           //Carga la variable day_num con el valor del vector solo si todos los elementos son iguales  
      fprintf(PORT1,"day number1###### =%ld\n\r",day_num);
   }
   
   else 
   {
      day_num = return_mode();                                                   // si los elementos no son todos iguales, carga usando la funcion return_mode                                                  
      // fprintf(PORT1,"day number1***** =%ld\n\r",day_num);
   }
 
   delay_ms(500);
   
   first_run_mode();                                                             //Main=ON, COMM=OFF, 3V3#1=ON, 3V3#2=ON, Unreg#1and#2=OFF

   while(TRUE)
   {
     delay_ms(1000); 
     resetData[0] = 0x8E;
     resetData[1] = sec;
     resetData[2] = min;
     resetData[3] = hou;   
      
     resetData[4] = (day_num >> 8) & 0xFF;
     resetData[5] = day_num & 0x00FF; 

     adcreading_reset();                                                         //Lee las entradas analogicas y carga en el vector
     
     if((min==30) && (hou <=1))                                                  //Cuando se cumpla los primeros 30 minutos
     {                                                                           
      RD6 = 1;                                                                   // Switch enable for COM PIC
      RC2 = 1;                                                                   //Buckboost enable for Unreg #1 (VHF/UHF Tranceiver)
     printf("******     COM PIC ON     *********\n\r");
     printf("******     VHF/UHF TRANCEIVER ON     *********\n\r");
     delayloop(60);                                                              //temporiza 60 segundos

     }
     
 //===============================UART WITH MAIN PIC==========================      
         for(int32 num = 0; num < 100; num++)                                                      
         {                                                                       
           if(kbhit(MPIC))                                                       //Devuelve verdadero si un caracter ha sido recibido
           {                                                                     //y espera en el buffer para ser leido
               MAIN_ACK = fgetc(MPIC);   //UART receive from main pic            //Lee el caracter del buffer
               break;
           }                                                                     
                                                                                 
         }
//======================================================================================================= 

         if(MAIN_ACK == 0x28)                                                    //receiving command from main pic for sending reset data
         {   
           
            fprintf(PORT1,"******Sending data to main *********\n\r");
            MAIN_ACK=0;
            main_count = 0;
            
            for(int i=0;i<11;i++)                                                //Se envian uno por uno los datos del vector resetData
            { 
               fputc(resetData[i],MPIC);  
               //delay_ms(10);              
            }
            fprintf(PORT1,"******data sent *********\n\r");
          }       
            
         else if(MAIN_ACK == 0xBC)                                               //receiving command from main pic for turning on Unreg 2
          {            
             fprintf(PORT1,"******BURNER CIRCUIT ON *********\n\r");
             MAIN_ACK=0;
             main_count = 0;
             RC5 = 1;                                                            //Enable Unreg#2
             fputc(0xCB,MPIC);                                                   //Unreg 2 On signal to Main PIC
             delayloop(45);                                                      //delay de 45 segundos
             RC5 = 0;                                                            //Disable Unreg#2
             fprintf(PORT1,"******BURNER CIRCUIT OFF *********\n\r");
          }      
          
        else if(MAIN_ACK == 0x27)                                                //receiving command from main pic for resetting the system
          {            
             MAIN_ACK=0;
             main_count = 0;
             fprintf(PORT1,"******Received command for satellite reset *********\n\r");
             reset_mode();
          }   
          
        else if(main_count >= 7200)                                              //Resetea el Main PIC cada 2 horas (main_count >= 7200)
           {
            resetMain();
           }
 //=================================24hr reset part=========================================================================      
         if(res_flag == 1)                                                       //Si la bandera de reseteo esta en alto
         {
            day_num_write();                                                     //guarda en la EEPROM el valor de la variable day_num
            RD1 = 0;                                                             //3V3#1 turned off first (to prevent FAB interrupt)
            do
            {
               fputc(0xAA,MPIC);                                                 //Se envia comando al Main PIC para guardar datos previo al reseteo
                 
            }while(fgetc(MPIC) == 0x27);                                         //Espera confirmacion del MAin PIC para resetear el sitema
            
            //tel mpic to wait delay 20ms
            fprintf(PORT1,"******Received acknoledgement for reset*********\n\r");
            
             MAIN_ACK =0;
             main_count = 0;
             reset_mode();                                                       //Funcion de reseteo del sistema
             res_flag = 0;                                                       
         }
         
   }

}
