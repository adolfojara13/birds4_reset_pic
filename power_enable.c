void delayloop(int32 seconds)                                                    //Funcion de temporizacion
{                                                                                //Recibe un int32 con el que puede temporizar
   int32 count = 1;                                                              //De 0 a 4.294.967.295 segundos
  
   while(count <= seconds) 
   { 
    delay_ms(1000); 
    count++; 
   } 
    
 return; 
}

void first_run_mode(void)                                                        //Funcion que se llama para la primera ejecucion
{                                                                                //all missions ON

    fprintf(PORT1,"******Reset first run enabled*********\n\r");
    RD6 = 0;                                                                     // Switch enable for COM PIC
    fprintf(PORT1,"******COM PIC OFF *********\n\r");
    //RD7 = 1;                                                                     // Switch enable for Main PIC
    RD7 = 0;
    fprintf(PORT1,"******Main PIC ON *********\n\r");
    
    RC4 = 1;                                                                     // Converter enable for 3V3 #1 
    RD1 = 1;                                                                     // Switch enable for 3V3 #1 (OCP 2A)
    
    RD3 = 1;                                                                     // Converter enable for 3V3 #2
    RD0 = 1;                                                                     // Switch enable for 3V3 #2 (OCP 2A)
     
    RC2 = 0;                                                                     //OCP switch disable for Unreg #1
    
    RC5 = 0;                                                                     //OCP switch disable for Unreg #2
   
   
    return;
 
}



void reset_mode(void)                                                            //Funcion Modo Reset
{                                                                                //Se resetean COM, MAIN y las alimentaciones 3V3 #1 y #2
    fprintf(PORT1,"******Reset mode *********\n\r"); 
    RD6 = 0;                                                                     // Switch disable for COM PIC
    RD7 = 0;                                                                     // Switch disable for Main PIC
    
    RD1 = 0;                                                                     // OCP disable  for 3V3#1
    RD0 = 0;                                                                     // OCP disable for 3V3 #2
    RC2 = 0;                                                                     // OCP disable for Unreg #1
    
    fprintf(PORT1,"****** SYSTEM OFF *********\n\r");
    delay_ms(5000);
     
    
    RD6 = 1;                                                                     // Switch enable for COM PIC
    //RD7 = 1;                                                                     // Switch enable for Main PIC
    
    RC4 = 1;                                                                     // Converter enable for 3V3 #1
    RD1 = 1;                                                                     // OCP enable for 3V3#1
    RD3 = 1;                                                                     // Converter enable for 3V3 #2
    RD0 = 1;                                                                     // OCP enable for 3V3 #2
    
    RC2 = 1;                                                                     // OCP enable for Unreg #1 (TRANSCEIVER VHF/UHF)
    RC5 = 0;                                                                     // OCP enable for Unreg #2 (BURNER CIRCUIT)
    fprintf(PORT1,"****** SYSTEM ON *********\n\r");
    //res_flag = 0;
    
    return; 
}



void resetMain(void)                                                             //Funcion para Reseteo del Main PIC
{
    RD7 = 0;                                                                     // Switch disable for Main PIC
    delay_ms(1000);
    //RD7 = 1;                                                                     // Switch enable for Main PIC
    fprintf(PORT1,"******Main PIC Reset *********\n\r");
    main_count  = 0;                                                             //se pone a cero el contador principal
   
    return;
}


