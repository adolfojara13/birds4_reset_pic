#define day1H 5                                                                  //El dato "dia" de la variable day_num es un dato de tipo long que se guarda  
#define day1L 8                                                                  //en 10 posiciones diferentes de la EEPROM para evitar perdida de dato causado 
                                                                                 //por la radiacion por lo que es necesario definir 20 posiciones de memoria
#define day2H 10                                                                 //para almacenar los datos High y Low respectivamente
#define day2L 11

#define day3H 15
#define day3L 16

#define day4H 20
#define day4L 21

#define day5H 25
#define day5L 26

#define day6H 30
#define day6L 31

#define day7H 35
#define day7L 36

#define day8H 40
#define day8L 41

#define day9H 45
#define day9L 46

#define day10H 50
#define day10L 51

//int8 daycount = 5;
int16 dy[10]={0};                                                                //Vector de tipo long de 10 posiciones para manejar el dato "dia" de la variable day_num 
//unsigned int16 day_num = 0;                                                    //almacenado en la EEPROM. El valor de day_num puede ir de 0 a 65.535

void day_num_write(void)                                                         //Funcion de escritura de la EEPROM, escribe el dato "dia" contenido en la variable day_num
{                                                                                //en 10 posiciones diferentes de la EEPROM dividiendo el dato en dayH y dayL
   write_eeprom(day1H, (day_num >>8) & 0xff);
   write_eeprom(day1L, day_num & 0xff);

   write_eeprom(day2H, (day_num >>8) & 0xff);
   write_eeprom(day2L, day_num & 0xff);

   write_eeprom(day3H, (day_num >>8) & 0xff);
   write_eeprom(day3L, day_num & 0xff);

   write_eeprom(day4H, (day_num >>8) & 0xff);
   write_eeprom(day4L, day_num & 0xff);

   write_eeprom(day5H, (day_num >>8) & 0xff);
   write_eeprom(day5L, day_num & 0xff);
   
   write_eeprom(day6H, (day_num >>8) & 0xff);
   write_eeprom(day6L, day_num & 0xff);
   
   write_eeprom(day7H, (day_num >>8) & 0xff);
   write_eeprom(day7L, day_num & 0xff);
   
   write_eeprom(day8H, (day_num >>8) & 0xff);
   write_eeprom(day8L, day_num & 0xff);
   
   write_eeprom(day9H, (day_num >>8) & 0xff);
   write_eeprom(day9L, day_num & 0xff);
   
   write_eeprom(day10H, (day_num >>8) & 0xff);
   write_eeprom(day10L, day_num & 0xff);
   
   write_eeprom(day1H, (day_num >>8) & 0xff);
   write_eeprom(day1L, day_num & 0xff);
   
   return;
}


void day_read()                                                                  //Funcion para leer la variable dia almacenada en 10 posiciones diferentes de la EEPROM
{                                                                                //La funcion make16() convierte a long el dato leido de las posiciones de memoria 
                                                                                 //indicadas por dayH y dayL y lo guarda en el vector dy[] 
   dy[0] = make16( read_eeprom(day1H),read_eeprom(day1L) );
   //fprintf(PORT1,"day number1 =%04ld\n\r",dy[0]);
   dy[1] = make16( read_eeprom(day2H),read_eeprom(day2L) );   
   //fprintf(PORT1,"day number2 =%04ld\n\r",dy[1]);
   dy[2] = make16( read_eeprom(day3H),read_eeprom(day3L) );
   //fprintf(PORT1,"day number3 =%04ld\n\r",dy[2]);
   dy[3] = make16( read_eeprom(day4H),read_eeprom(day4L) );  
   //fprintf(PORT1,"day number4 =%04ld\n\r",dy[3]);
   dy[4] = make16( read_eeprom(day5H),read_eeprom(day5L) );
   //fprintf(PORT1,"day number5 =%04ld\n\r",dy[4]);
   dy[5] = make16( read_eeprom(day6H),read_eeprom(day6L) );
   //fprintf(PORT1,"day number1 =%04ld\n\r",dy[5]);
   dy[6] = make16( read_eeprom(day7H),read_eeprom(day7L) );   
   //fprintf(PORT1,"day number2 =%04ld\n\r",dy[6]);
   dy[7] = make16( read_eeprom(day8H),read_eeprom(day8L) ); 
   //fprintf(PORT1,"day number3 =%04ld\n\r",dy[7]);
   dy[8] = make16( read_eeprom(day9H),read_eeprom(day9L) );   
   //fprintf(PORT1,"day number4 =%04ld\n\r",dy[8]);
   dy[9] = make16( read_eeprom(day10H),read_eeprom(day10L) );
   //fprintf(PORT1,"day number5 =%04ld\n\r",dy[9]);
   
   return;
}

 int16 return_mode()                                                             //Funcion que devuelve el valor que mas se repite en el vector
{                                                                                //que contiene el dato "dia"
    int16 returnVal = dy[0];                                                     // almacena el dato que sera devuelto
    int repeatCount = 0;                                                         // cuenta el numero de repeticiones
    int prevRepCnt = 0;                                                          // almacena el ultimo mayor numero de repeticiones 

    for (int i=0; i<10; i++) {                                                   // recorre todos los elementos a analizar

        for (int j=i; j<10; j++) {                                               // recorre por todos los elementos posteriores al elemento analizado

            if (i != j && dy[i] == dy[j]) {                                      // si se encuentran coincidencias
                repeatCount++;                                                   // incrementa el contador de repeticiones  
            }                                                          
        }
        if (repeatCount>=prevRepCnt) {                                           // si el elemento actual tiene mayor numero de repeticiones
             returnVal=dy[i];                                                    // guarda para devolver despues el elemento de esa posicion
             prevRepCnt = repeatCount;                                           // guarda el numero de repeticiones 
        }        
        repeatCount=0;                                                           // resetea el contador para la siguiente comparacion
    }
    return returnVal;                                                            //retorna el valor de mayor repeticiones
}

