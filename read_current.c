int8 READ_PIC_ADC(int8 port_name)                                                //Funcion para uso del conversor A/D
{
   SET_ADC_CHANNEL(port_name);                                                   //Seleccion del canal analogico
   delay_us(20);                                                                 //delay necesario para el tiempo de adquisicion tipico=20us
   int8 ADC_VALUE = READ_ADC();                                                  //Lectura analogica, resultado es un entero de 16bits
   return ADC_VALUE;                                                             //Devuelve el valor de la lectura
}

void adcreading_reset(void)                                                      //Funcion de lectura de parametros analogicos controlados por el Reset PIC
{                                                                                //Los parametros leidos se almacenan en posiciones del vector resetData
   //fprintf(PORT1,"******ADC reading start *********\n\r");
   
   resetData[6] = READ_PIC_ADC(0);                                               //V out raw
              
   resetData[7] = READ_PIC_ADC(1);                                               //I out 3V3 #1 
      
   resetData[8] = READ_PIC_ADC(2);                                               //I out 3V3 #2
        
   resetData[9] = READ_PIC_ADC(4);                                               //I out unreg #1 
        
   resetData[10] = READ_PIC_ADC(5);                                              //I out unreg #2   
        
   return;
        
}


